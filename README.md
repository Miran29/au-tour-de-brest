<div align="center">
    <img src="./logo.png" width=200px/>
</div>

## Aperçu de l'application

Une vidéo de démonstration est disponible sur YouTube via ce lien : [Au Tour De Brest Démo](https://www.youtube.com/watch?v=BJzLDfEG93o)

## À propos

Au Tour De Brest est une application développée, en une dizaine de jours, par moi-même ainsi que 4 des mes camarades de classe dans le cadre de notre module "Conduite de Projet Objet" du semestre 6 à l'Ecole Nationale d'Ingénieurs de Brest (ENIB).

Cette application permet de découvrir l'histoire de deux lieux incontournables de la ville de Brest (le château de Brest & les Capucins) à travers une modélisation en trois dimensions.

En plus de l'application PC, nous avons eu l'opportunité d'étendre cette application dans un casque de réalité virtuelle (Honor Vive avec ses manettes via SteamVR SDK dans Unity) au Centre Européen de Réalité Virtuelle.

#### Le château de Brest
Dans la zone de modélisation du château, plusieurs images  sont disposées en l'air. Quand l'utilisateur se trouve au-dessous d'une de ces images, un texte apparaît pour en apprendre plus sur l'histoire du château.<br>

La visite du château peut se faire aussi bien seul qu'avec l'aide d'un guide virtuel.<br>

Ce guide virtuel a été implémenté dans le but de suivre un chemin passant au-dessous de chacune des images dans l'ordre chronologique. Pour le solliciter, il suffit de s'approcher de lui et il commencera alors à se déplacer.
Dès lors qu'il se trouve au-dessous d'une image, il s'arrête. Il attendra alors que l'utilisateur se place sous l'image et qu'il lise le texte associé qui lui est associé.
Pour que le guide reprenne son chemin, il faudra s'éloigner un peu de l'image (jusqu'à ce que le texte disparaisse).<br>

De plus, on retrouve des sphères sous lesquels il est possible d'interagir à l'aide de la touche **E**. L'action alors effectuée est une simple téléportation (sur une tour du château, dans une nouvelle zone...).

#### Les Capucins
Dans cette représentation 3D, une exposition se trouve dans le bâtiment principal. Pour entre dans ce dernier, il faudra interagir, avec la touche **E**, lorsque le personnage se trouvera sous l'une des multiples sphères se trouvant dans les airs.<br>

Une fois à l'intérieur, une multitude d'images seront présentes dans l'allée centrale. Pour en apprendre plus sur une des images, il faudra se placer devant l'image.

#### Se déplacer entre les lieux
Pour se déplacer entre les Capucins et le château de Brest, il faudra tout d'abord trouver une modélisation du tramway de la ville de Brest. Une fois trouvé, il faudra rentrer dans ce dernier.<br>

L'utilisateur se téléportera alors sur une ligne de tramway. Il verra alors, devant lui, le tramway. Pour le piloter, il faut appuyer sur la touche **E**. Le tramway se pilote de la même manière que le personnage.<br>

Sur cette ligne de tramway, le but est d'arriver au terminus (les Capucins ou le château en fonction du lieu de départ).
Sur le trajet, de nouvelles images représentant d'autres lieux emblématiques de la ville de Brest sont présentes. Pour en apprendre plus sur ces lieux, il faudra se coller à l'image et appuyer sur la touche **I**. En s'éloignant de l'image, le texte disparaîtra.

## Tester l'application sur sa machine personnelle 

Prérequis: Un environnement Windows.

Étapes à suivre :<br>
    1. Se rendre sur ce Google Drive : https://drive.google.com/drive/folders/1zU4IY7hzkiSCslOCo0yNANgbAwUNaRa9?usp=sharing<br>
    2. Télécharger l'archive "Executable.zip"<br>
    3. Décompresser cette dernière<br>
    4. Lancer l'exécutable nommé "Au Tour De Brest"<br>
    5. Optionnel: au lancement, il est possible de modifier des paramètres graphiques<br>
    6. Appuyer sur Play!<br>
    7. Choisir l'endroit qu'on souhaite visiter (les Capucins ou le château)

### Contrôles
    ZQSD : déplacer le personnage ou le tramway (en fonction de la zone)
    Left Shift + Z : déplacer le personnage vers l'avant en courant
    Avec la souris : rotation du personnage
    Tab: activer la vue à la 1ère personne
    A: désactiver la vue à la 1ère personne en repassant en vue 3ème personne
    E ou I: en fonction de la zone, touche d'interaction pour différentes actions (se téléporter, afficher du texte, rentrer dans le tram...)

_**Pour plus d'informations, se référer au pdf disponible sur ce repository.**_

> **Application développé par  :** Amoev Miran, Bihan Pierre, Piecuch Louise, Poadi Timothée, Pujar Théodore 

> **Au Tour De Brest**, Projet Informatique du Semestre 6, Juin 2019, ENIB